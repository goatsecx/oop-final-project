#pragma once

#if defined WIN32
#include <freeglut.h>
#elif defined __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif

#include "glut_window.h"
#include "Header.h"

//#define NUM_TYPES 21

class GamePiece
{
	//num types definition
public: 
	static const int NUM_TYPES = 21;
    static const int zero = 0;

	static enum Types {
		EMPTY = 0x0,
		OCCUPIED = 0x1,
		CORNER = 0x2
	};
    
private:
	
	Vec pos;            // Position Data
	Color color;        // color
	int owner;          // owner
    int rotation;
    
    void doRotation(int i, int j, int &x, int &y); //tranforms coordinates based on rotation

public:
    static char types[NUM_TYPES][5][5]; // Type Data
    
    unsigned int type;  // Piece Type
    
	GamePiece();
	GamePiece(unsigned int type, int owner);
	~GamePiece();

	void draw();
	void update(const GlutWindow::Event& e);
    void rotate( int changeinrot); //rotating piece function
    char getData(int i, int j); //will translate and then get data from the piece;gets piece data accounting for rotation

	void setPosition(Vec vec);
	Vec getPosition();
	void setColor(Color color);
	Color getColor();
    
};

