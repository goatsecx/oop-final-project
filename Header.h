#pragma once
#include <iostream>
#include <vector>
using namespace std;

class Vec {
public:
	int x;
	int y;

	Vec() {
		x = y = 0;
	}

	Vec(int x, int y) {
		this->x = x;
		this->y = y;
	}
};

class Color {
public:
	float r;
	float g;
	float b;

	Color() {
		r = (float)(rand() % 256) / 256;
		g = (float)(rand() % 256) / 256;
		b = (float)(rand() % 256) / 256;
	}

	Color(float r, float g, float b) {
		this->r = r;
		this->g = g;
		this->b = b;
	}

	Color(const Color &c){
		r = c.r;
		g = c.g;
		b = c.b;
	}
};

class GrRect {
	public:
		static const int MOVE_MARGIN = 50;

		vector<Vec> points;
		bool active;
		bool drug;
		int clickOffsetX;
		int clickOffsetY;
		int x;
		int y;
		int w;
		int h;
		Color color;
		
	GrRect(){
		drug = active = false;
		clickOffsetX = 0;
		clickOffsetY = 0;
		x = 0;
		y = 0;
		w = 0;
		h = 0;
	}

	GrRect(int x, int y, int w, int h) : color() {
		drug = active = false;
		clickOffsetX = 0;
		clickOffsetY = 0;
		this->x = x;
		this->y = y;
		this->w = w;
		this->h = h;
	}

	GrRect(int x, int y, int w, int h, Color color) : color(color) {
		drug = active = false;
		clickOffsetX = 0;
		clickOffsetY = 0;
		this->x = x;
		this->y = y;
		this->w = w;
		this->h = h;
	}

	GrRect(const GrRect &r){
		drug = active = false;
		clickOffsetX = 0;
		clickOffsetY = 0;
		x = r.x;
		y = r.y;
		w = r.w;	
		h = r.h;
	}

	int contains(Vec v){
		if(v.x > x && v.x < x+w && v.y > y && v.y < y+h)
            return 1;
        return 0;
	}

	void drag(bool drug){
		this->drug = drug;
	}
	bool isDragged(){
		return drug;
	}

	void focus(bool active) {
		this->active = active;
	}

	bool isFocused(){
		return active;
	}

	void setPosition(int x, int y) {
		this->x = x;
		this->y = y;
	}

	virtual void draw() {
		glBegin(GL_QUADS); // piece #1
		glColor3d(1.0, 0.9, 0.3);
		glVertex2d(-90, -80);
		glVertex2d(-90, -70);
		glVertex2d(-80, -70);
		glVertex2d(-80, -80);
		glEnd();

		/*glBegin(GL_LINES);
		glColor3d ( 0.0, 0.0, 0.0 );	
		glVertex2d(x, y);
		glVertex2d(x + w, y);
		glVertex2d(x + w, y);
		glVertex2d(x + w, y + h);
		glVertex2d(x + w, y + h);
		glVertex2d(x, y + h);
		glVertex2d(x, y + h);
		glVertex2d(x, y);
		glEnd();

		glBegin(GL_POINTS);
		glColor3d(0.0, 1.0, 1.0);
		for(unsigned int i = 0; i < points.size(); i++){
			glVertex2d(x + points.at(i).x, y + points.at(i).y);
		}
		glEnd();

		

		glBegin(GL_QUADS);
		glColor3d(1.0, 1.0, 1.0);
		glVertex2d(x, y+h - 50);
		glVertex2d(x + w, y + h - 50);
		glVertex2d(x + w, y + h);
		glVertex2d(x, y + h);
		glColor3d(color.r, color.g, color.b);
		glVertex2d(x, y);
		glVertex2d(x + w, y);
		glVertex2d(x + w, y + h);
		glVertex2d(x, y + h);
		glEnd();
		*/

		
	}

	virtual void handle(const GlutWindow::Event& e) {
		points.push_back(Vec(e.mx - x, e.my - y));
	}
	

};

class SpazWindow : public GrRect {
public:
	SpazWindow() : GrRect() {
	}

	SpazWindow(int x, int y, int w, int h) : GrRect(x, y, w, h) {
	}

	SpazWindow(int x, int y, int w, int h, Color color) : GrRect(x, y, w, h, color) {
	}

	SpazWindow(const GrRect &r) : GrRect(r){
	}

	virtual void handle(const GlutWindow::Event& e) {
		color = Color();
	}
};

/*class CircleWin: public AppWin {
	public:
	CircleWin() : AppWin(){
	}

	CircleWin(int x, int y, int w, int h) : AppWin(x, y, w, h){
	}

	CircleWin(const GrRect &r) : AppWin(r) {
	}

	virtual void resize(int w, int h){
		erect.w = w;
		erect.h = h;

		if(erect.w < erect.h)
			cout << "radius: " << erect.w << endl;	
		else
			cout << "radius: " << erect.h << endl;
	}
	
};

class RectWin: public AppWin {
	public: 
	RectWin() : AppWin() {
	}

	RectWin(int x, int y, int w, int h) : AppWin(x, y, w, h){
	}

	RectWin(const GrRect &r) : AppWin (r){
	}

	virtual void resize(int w, int h){
		erect.w = w;
		erect.h = h;
		cout << "area: " << erect.w*erect.h << endl;
	}

};*/
