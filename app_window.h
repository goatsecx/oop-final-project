
// Ensure the header file is included only once in multi-file projects
#ifndef APP_WINDOW_H
#define APP_WINDOW_H

#include <vector>
# include "glut_window.h"
#include "Header.h"
#include "GameBoard.h"

// The functionality of your application should be implemented inside AppWindow
class AppWindow : public GlutWindow
{
private:
	enum MenuEv { evOption0, evOption1 };
	int _markx, _marky;
	int _w, _h;

	GameBoard gameBoard;

public:
	AppWindow(const char* label, int x, int y, int w, int h);
	void windowToScene(int& x, int& y);

private: // functions derived from the base class
	virtual void handle(const Event& e);
	virtual void draw();
	virtual void resize(int w, int h);


};

#endif // APP_WINDOW_H
