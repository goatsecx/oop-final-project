
# include <iostream>
# include "app_window.h"


//==========================================================================
// Main routine
//==========================================================================
int main ( int argc, char** argv )
 {
   // Init freeglut library:
   glutInit ( &argc,argv );
   glutInitDisplayMode ( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );

   // Now create the window of your application:
   AppWindow* w = new AppWindow ( "Team Goatse.cx", 300, 10, 700, 700 ); // gameboard: 700x700 pixels = 20x20 squares
																		//make squares 35x35 pixels each

   // Finally start the main loop:
   w->run ();
}
