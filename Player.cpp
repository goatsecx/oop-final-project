#include "Player.h"
using namespace std;


Player::Player(int playerID)
{
    this->playerID = playerID;
	score = 0;
	activePiece = 0;
	for (int i = 0; i < GamePiece::NUM_TYPES; i++){
		hand.push_back(new GamePiece(i, playerID));
	}
}


void Player::selectPieceDown(){
	activePiece--;
	if (activePiece < 0)
		activePiece = hand.size() - 1;
}

void Player::selectPieceUp(){
	activePiece++;
	if (activePiece >= hand.size())
		activePiece = 0;
}

GamePiece* Player::playPiece(){
	GamePiece* playingPiece = hand.at(activePiece);
	hand.erase(hand.begin()+activePiece);
	activePiece %= hand.size();
	return playingPiece;
}

GamePiece* Player::getActivePiece(){ // returns activePiece thats going to be played, used in update in GameBoard to set the coordinates
	if (hand.size() == 0)
		return NULL;
	return hand.at(activePiece);
}

int Player::getPlayerMask() {
	return 0x1 << (playerID-1);
}

Player::~Player()
{
}
