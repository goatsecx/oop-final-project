#include "Square.h"
using namespace std;

Square::Square(int x, int y){
	this->x = x;
	this->y = y;
	this->validCornerMask = 0;
	this->invalidPlacementMask = 0;
	sqNum = 0;
}

Square::Square(int x, int y, int sqNum){
	this->x = x;
	this->y = y;
	this->validCornerMask = 0;
	this->invalidPlacementMask = 0;
	this->sqNum = sqNum;
}