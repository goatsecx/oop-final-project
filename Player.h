#pragma once
#include "GamePiece.h"
#include <vector>
class Player
{
public:
    int playerID;
	int score;
    
    
	vector<GamePiece*> hand;
	int activePiece;
	Player(int playerID);
	GamePiece* playPiece();
	GamePiece* getActivePiece();
	void selectPieceUp();
	void selectPieceDown();
	int getPlayerMask();
	~Player();
};

