#include "GamePiece.h"
#include <exception>
using namespace std;

GamePiece::GamePiece()
{
	type = 0;
	pos = Vec();
	color = Color();
    rotation = 0;
}

GamePiece::GamePiece(unsigned int type, int owner) : GamePiece() {
	if (type > NUM_TYPES)
		throw exception();
	this->type = type;
    this->owner = owner;
    this->rotation = 0;
    switch (owner)      {
        case -1:
            color = Color(0.75, 0.75, 0.75); //setting the color of the current game piece based on the owner;stops the randomizing of color before playing the piece on the board

            break;
        case 1:
            color = Color(0.5, 0.0, 1.0);
            break;
        case 2:
            color = Color(1.0, 0.8, 0.0);
            break;
        default:
            color = Color(1.0, 1.0, 1.0); // white background
    }

    
    
}


GamePiece::~GamePiece()
{
}

void GamePiece::draw(){
	glColor3f(color.r, color.g, color.b);
	for (int j = 0; j < 5; j++) {
		for (int i = 0; i < 5; i++) {
			if (getData(i, j)) {
				glVertex2d(-25 + (i * 10) + pos.x, -25 + (j * 10) + pos.y);
				glVertex2d(-15 + (i * 10) + pos.x, -25 + (j * 10) + pos.y);
				glVertex2d(-15 + (i * 10) + pos.x, -15 + (j * 10) + pos.y);
				glVertex2d(-25 + (i * 10) + pos.x, -15 + (j * 10) + pos.y);
			}
		}
	}
}

void GamePiece::update(const GlutWindow::Event& e) { //looks like we didn't use this function, what did we originally implent it for? just curious -T
	/*pos.x = e.mx;
	pos.y = e.my;
	if (type == 0) {
		type = 1;
	}
	else {
		type = 0;
	}*/
}

char GamePiece::getData(int i, int j){
    int x, y;
    doRotation(i, j, x, y);
    return types[type][y][x];
}

void GamePiece::rotate( int changeinrot)
{
    rotation += changeinrot;
    rotation = rotation % 360;
    if(rotation < 0) {
        rotation += 360;
    }

}

void GamePiece::setPosition(Vec vec){
	pos = vec;
}

Vec GamePiece::getPosition(){
	return pos;
}

void GamePiece::setColor(Color col){
	color = col;
}

Color GamePiece::getColor(){
	return color;
}

void GamePiece::doRotation(int i, int j, int &x, int &y) {
    switch(this->rotation) {
        case 0:
            x = i;
            y = j;
            break;
        case 90:
            x = j;
            y = i;
            y = 4 - y;
            break;
        case 180:
            x = 4 - i;
            y = 4 - j;
            break;
        case 270:
            x = j;
            y = i;
            x = 4 - x;
            break;
        default:
            x = i;
            y = j;
            break;
    }
}

char GamePiece::types[NUM_TYPES][5][5] = {
		{// piece #1
			{ zero, zero, zero, zero, zero},
			{ zero, zero, zero, zero, zero},
			{ zero, zero, 3, zero, zero   },
			{ zero, zero, zero, zero, zero},
			{ zero, zero, zero, zero, zero}
		},
		{// piece #2
			{ zero, zero, zero, zero, zero  },
			{ zero, zero, zero, zero, zero  },
            { zero, zero, 3, 3, zero        },
			{ zero, zero, zero, zero, zero  },
			{ zero, zero, zero, zero, zero  }
		},
		{// piece #3
			{ zero, zero, zero, zero, zero  },
			{ zero, zero, zero, zero, zero  },
			{ zero, 3, 3, zero, zero        },
			{ zero, zero, 3, zero, zero     },
			{ zero, zero, zero, zero, zero  }
		},
		{// piece #4
			{ zero, zero, zero, zero, zero  },
			{ zero, zero, zero, zero, zero  },
			{ zero, 3, 1, 3, zero           },
			{ zero, zero, zero, zero, zero  },
			{ zero, zero, zero, zero, zero  }
		},
        { //piece #5
            { zero, zero, zero, zero, zero  },
            { zero, 3, 3, zero, zero        },
            { zero, 3, 3, zero, zero        },
            { zero, zero, zero, zero, zero  },
            { zero, zero, zero, zero, zero  }
        },
        { //piece #6
            {zero, zero, zero, zero, zero },
            { zero, zero, 3, zero, zero   },
            { zero, 3, 1, 3, zero         },
            {zero, zero, zero, zero, zero },
            { zero, zero, zero, zero, zero}
        },
        { //piece #7
            { zero, zero, zero, zero, zero  },
            { zero, zero, zero, zero, zero  },
            { 3, 1, 1, 3, zero              },
            { zero, zero, zero, zero, zero  },
            { zero, zero, zero, zero, zero  }
        },
        { //piece #8
            { zero, zero, zero, zero, zero  },
            { zero, zero, zero, 3, zero     },
            { zero, 3, 1, 3, zero           },
            { zero, zero, zero, zero, zero  },
            { zero, zero, zero, zero, zero  }
        },
        { //piece #9
            { zero, zero, zero, zero, zero  },
            { zero, zero, 3, 3, zero        },
            { zero, 3, 3, zero, zero        },
            { zero, zero, zero, zero, zero  },
            { zero, zero, zero, zero, zero  }
        },
        { //piece #10
            { zero, zero, zero, zero, zero  },
            { zero, 3, zero, zero, zero     },
            { zero, 3, 1, 1, 3              },
            { zero, zero, zero, zero, zero  },
            { zero, zero, zero, zero, zero  }
        },
        { //piece #11
            { zero, zero, 3, zero, zero    },
            { zero, zero, 1, zero, zero    },
            { zero, 3, 1, 3, zero          },
            { zero, zero, zero, zero, zero },
            { zero, zero, zero, zero, zero }
        },
    
		{ //piece #12
			{ zero, zero, 3, zero, zero    },
			{ zero, zero, 1, zero, zero    },
			{ zero, zero, 3, 1, 3          },
			{ zero, zero, zero, zero, zero },
			{ zero, zero, zero, zero, zero }
		},
        { //piece #13
            { zero, zero, zero, zero, zero},
            { zero, zero, 3, 1, 3         },
            { zero, 3, 3, zero, zero      },
            { zero, zero, zero, zero, zero},
            { zero, zero, zero, zero, zero}
        },
        { //piece #14
            { zero, zero, zero, zero, zero },
            { zero, zero, zero, 3, zero    },
            { zero, 3, 3, 3, zero          },
            { zero, 3, zero, zero, zero    },
            { zero, zero, zero, zero, zero }
        },
        { //piece #15
            { zero, zero, 3, zero, zero },
            { zero, zero, 1, zero, zero },
            { zero, zero, 1, zero, zero },
            { zero, zero, 1, zero, zero },
            { zero, zero, 3, zero, zero }
        },
        { //piece #16
            { zero, zero, zero, zero, 0    },
            { zero, 3, zero, zero, zero    },
            { zero, 1, 3, zero, zero       },
            { zero, 3, 3, zero, zero       },
            { zero, zero, zero, zero, zero }
        },
        { //piece #17
            { zero, zero, zero, zero, zero  },
            { zero, 3, 3, zero, zero        },
            { zero, 3, 3, zero, zero        },
            { zero, 3, zero, zero, zero     },
            { zero, zero, zero, zero, zero  }
        },
        { //piece #18
            { zero, zero, zero,zero, zero  },
            { zero, zero, 3, 3, zero       },
            { zero, zero, 1, zero, zero    },
            { zero, zero, 3, 3, zero       },
            { zero, zero, zero, zero, zero }
        },
        { //piece #19
            { zero, zero, zero, zero, zero  },
            { zero, zero, 3, 3, zero        },
            { zero, 3, 1, zero, zero        },
            { zero, zero, 3, zero, zero     },
            { zero, zero, zero, zero, zero  }
        },
        { //piece #20
            { zero, zero, zero, zero, zero  },
            { zero, zero, 3, zero, zero     },
            { zero, 3, 1, 3, zero           },
            { zero, zero, 3, zero, zero     },
            { zero, zero, zero, zero, zero  }
        },
        { //piece #21
            { zero, zero, zero, zero, zero  },
            { zero, zero, 3, zero, zero     },
            { zero, 3, 1, 1, 3              },
            { zero, zero, zero, zero, zero  },
            { zero, zero, zero, zero, zero  }
        }
    
};
//hi babe
