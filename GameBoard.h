#pragma once

#if defined WIN32
#include <freeglut.h>
#elif defined __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif

#include "GamePiece.h"
#include "Player.h"
#include "Square.h"

#include "glut_window.h"
#include <vector>

using namespace std;

class GameBoard
{
	//Player player;
	char * scoreStr;
	char * winnerStr;
	int consecutiveSkips;
	bool gameover;
	vector<Player*> players; //players who are in the game
	int activePlayer;
	vector <GamePiece*> pieces;
	int num_of_col = 16;
	int num_of_row = 16;
	vector<vector<Square*>> board; //14x14 gameboard, outside perimeter is the boundary
	//!!!!rows and columns iterate from 1-14!!!!!
	int init_value = 0;
	int const flag = -1; //flag for outside boundary
	
public:
	GameBoard();
	~GameBoard();

	void selectPlayer();
	bool gameIsNotOver();
	Player* getActivePlayer();
	Square* getSquare(int x, int y);
    bool TryPlacePiece(GamePiece* piece);
	void draw();
	void update(const GlutWindow::Event& e); //updates after every mouse click
	void printScores();
	void drawGameOver();
};

