
# include <iostream>
# include "app_window.h"

AppWindow::AppWindow(const char* label, int x, int y, int w, int h)
	:GlutWindow(label, x, y, w, h)
{
	_markx = 0;
	_marky = 0;
	addMenuEntry("Option 0", evOption0);
	addMenuEntry("Option 1", evOption1);
	gameBoard.selectPlayer();

}

// mouse events are in window coordinates, but your scene is in [0,1]x[0,1],
// so make here the conversion when needed
void AppWindow::windowToScene(int& x, int &y)
{
	x = ((float)x / _w) * 160 - 160 / 2;
	y = ((float)-y / _h) * 160 + 160 / 2;
}

// Called every time there is a window event
void AppWindow::handle(const Event& e)
{
	windowToScene(e.mx, e.my);

	//printf("X %d Y %d\n", e.mx, e.my);

	bool rd = true;

	gameBoard.update(e);

	/*if (e.type == Keyboard) {
		switch (e.key)
		{
		case ' ': // space bar
			std::cout << "Space pressed.\n";
			_markx = 1.5;
			_marky = 1.5;
			redraw();
			break;

		case 27: // Esc was pressed
			exit(1);
		}
	}

	if (e.type == Menu)
	{
		std::cout << "Menu Event: " << e.menuev << std::endl;
		rd = false; // no need to redraw
	}

	const float incx = 0.02f;
	const float incy = 0.02f;
	if (e.type == SpecialKey)
		switch (e.key)
	{
		case GLUT_KEY_LEFT:  _markx -= incx; break;
		case GLUT_KEY_RIGHT: _markx += incx; break;
		case GLUT_KEY_UP:    _marky += incy; break;
		case GLUT_KEY_DOWN:  _marky -= incy; break;
		default: rd = false; // no redraw
	}*/

	if (rd) redraw(); // ask the window to be rendered when possible
}

void AppWindow::resize(int w, int h)
{
	// Define that OpenGL should use the whole window for rendering
	glViewport(0, 0, w, h);
	_w = w; _h = h;
}

// here we will redraw the scene according to the current state of the application.
void AppWindow::draw()
{
	// Clear the rendering window
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Clear the trasnformation stack
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	/*  glBegin(GL_LINES);
	  glColor3d(0.0, 0.0, 0.0);
	  glVertex2d(x, y);
	  glVertex2d(x + w, y);
	  glVertex2d(x + w, y);
	  glVertex2d(x + w, y + h);
	  glVertex2d(x + w, y + h);
	  glVertex2d(x, y + h);
	  glVertex2d(x, y + h);
	  glVertex2d(x, y);
	  glEnd();*/

	/*glBegin(GL_QUADS); // piece #1
	glColor3d(1.0, 0.9, 0.3);
	glVertex2d(-90, -80);
	glVertex2d(-90, -70);
	glVertex2d(-80, -70);
	glVertex2d(-80, -80);
	glEnd();*/

	/*glBegin(GL_QUAD_STRIP); // piece #2
	glColor3d(1.0, 0.0, 0.0);
	glVertex2d(-100, -100);
	glVertex2d(-100, -90);
	glVertex2d(-90, -100);
	glVertex2d(-90, -90);


	glVertex2d(-90, -100);
	glVertex2d(-90, -90);
	glVertex2d(-80, -100);
	glVertex2d(-80, -90);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #3
	glColor3d(0.0, 1.0, 0.0);
	glVertex2d(-100, -80);
	glVertex2d(-100, -70);
	glVertex2d(-90, -80);
	glVertex2d(-90, -70);


	glVertex2d(-100, -90);
	glVertex2d(-100, -80);
	glVertex2d(-90, -90);
	glVertex2d(-90, -80);


	glVertex2d(-90, -90);
	glVertex2d(-90, -80);
	glVertex2d(-80, -90);
	glVertex2d(-80, -80);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #4
	glColor3d(0.0, 0.0, 1.0);
	glVertex2d(-100, -70);
	glVertex2d(-100, -60);
	glVertex2d(-90, -70);
	glVertex2d(-90, -60);


	glVertex2d(-100, -60);
	glVertex2d(-100, -50);
	glVertex2d(-90, -60);
	glVertex2d(-90, -50);


	glVertex2d(-100, -50);
	glVertex2d(-100, -40);
	glVertex2d(-90, -50);
	glVertex2d(-90, -40);

	glEnd();

	glBegin(GL_QUADS); // piece #5
	glColor3d(1.0, 0.0, 1.0);
	glVertex2d(-100, -40);
	glVertex2d(-100, -20);
	glVertex2d(-80, -20);
	glVertex2d(-80, -40);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #6
	glColor3d(0.0, 1.0, 1.0);
	glVertex2d(-80, -100);
	glVertex2d(-80, -90);
	glVertex2d(-70, -100);
	glVertex2d(-70, -90);

	glVertex2d(-70, -100);
	glVertex2d(-70, -90);
	glVertex2d(-60, -100);
	glVertex2d(-60, -90);

	glVertex2d(-70, -90);
	glVertex2d(-70, -80);
	glVertex2d(-60, -90);
	glVertex2d(-60, -80);

	glVertex2d(-60, -100);
	glVertex2d(-60, -90);
	glVertex2d(-50, -100);
	glVertex2d(-50, -90);

	glEnd();

	glBegin(GL_QUADS); // piece #7
	glColor3d(1.0, 1.0, 0.6);
	glVertex2d(-100, -20);
	glVertex2d(-100, 20);
	glVertex2d(-90, 20);
	glVertex2d(-90, -20);
	glEnd();


	glBegin(GL_QUAD_STRIP); // piece #8
	glColor3d(1.0, 0.6, 1.0);
	glVertex2d(-50, -100);
	glVertex2d(-50, -90);
	glVertex2d(-40, -100);
	glVertex2d(-40, -90);

	glVertex2d(-40, -100);
	glVertex2d(-40, -90);
	glVertex2d(-30, -100);
	glVertex2d(-30, -90);

	glVertex2d(-30, -100);
	glVertex2d(-30, -90);
	glVertex2d(-20, -100);
	glVertex2d(-20, -90);

	glVertex2d(-30, -90);
	glVertex2d(-30, -80);
	glVertex2d(-20, -90);
	glVertex2d(-20, -80);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #9
	glColor3d(0.6, 1.0, 1.0);
	glVertex2d(-20, -100);
	glVertex2d(-20, -90);
	glVertex2d(-10, -100);
	glVertex2d(-10, -90);

	glVertex2d(-10, -100);
	glVertex2d(-10, -90);
	glVertex2d(0, -100);
	glVertex2d(0, -90);

	glVertex2d(-10, -90);
	glVertex2d(-10, -80);
	glVertex2d(0, -90);
	glVertex2d(0, -80);

	glVertex2d(0, -90);
	glVertex2d(0, -80);
	glVertex2d(10, -90);
	glVertex2d(10, -80);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #10
	glColor3d(0.9, 0.0, 1.0);
	glVertex2d(10, -90);
	glVertex2d(10, -80);
	glVertex2d(20, -90);
	glVertex2d(20, -80);

	glVertex2d(10, -100);
	glVertex2d(10, -90);
	glVertex2d(20, -100);
	glVertex2d(20, -90);

	glVertex2d(20, -100);
	glVertex2d(20, -90);
	glVertex2d(30, -100);
	glVertex2d(30, -90);

	glVertex2d(30, -100);
	glVertex2d(30, -90);
	glVertex2d(40, -100);
	glVertex2d(40, -90);

	glVertex2d(49, -100);
	glVertex2d(40, -90);
	glVertex2d(50, -100);
	glVertex2d(50, -90);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #11
	glColor3d(0.9, 0.9, 1.0);
	glVertex2d(-10, -80);
	glVertex2d(-10, -70);
	glVertex2d(0, -80);
	glVertex2d(0, -70);

	glVertex2d(0, -80);
	glVertex2d(0, -70);
	glVertex2d(10, -80);
	glVertex2d(10, -70);

	glVertex2d(0, -70);
	glVertex2d(0, -60);
	glVertex2d(10, -70);
	glVertex2d(10, -60);

	glVertex2d(0, -60);
	glVertex2d(0, -50);
	glVertex2d(10, -60);
	glVertex2d(10, -50);

	glVertex2d(10, -80);
	glVertex2d(10, -70);
	glVertex2d(20, -80);
	glVertex2d(20, -70);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #12
	glColor3d(0.6, 0.9, 0.3);

	glVertex2d(20, -70);
	glVertex2d(20, -60);
	glVertex2d(30, -70);
	glVertex2d(30, -60);

	glVertex2d(20, -80);
	glVertex2d(20, -70);
	glVertex2d(30, -80);
	glVertex2d(30, -70);

	glVertex2d(20, -90);
	glVertex2d(20, -80);
	glVertex2d(30, -90);
	glVertex2d(30, -80);

	glVertex2d(30, -90);
	glVertex2d(30, -80);
	glVertex2d(40, -90);
	glVertex2d(40, -80);

	glVertex2d(40, -90);
	glVertex2d(40, -80);
	glVertex2d(50, -90);
	glVertex2d(50, -80);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #13
	glColor3d(0.4, 0.4, 0.8);

	glVertex2d(30, -80);
	glVertex2d(30, -70);
	glVertex2d(40, -80);
	glVertex2d(40, -70);

	glVertex2d(40, -80);
	glVertex2d(40, -70);
	glVertex2d(50, -80);
	glVertex2d(50, -70);

	glVertex2d(40, -70);
	glVertex2d(40, -60);
	glVertex2d(50, -70);
	glVertex2d(50, -60);

	glVertex2d(50, -70);
	glVertex2d(50, -60);
	glVertex2d(60, -70);
	glVertex2d(60, -60);

	glVertex2d(60, -70);
	glVertex2d(60, -60);
	glVertex2d(70, -70);
	glVertex2d(70, -60);

	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #14
	glColor3d(0.4, 0.8, 0.4);

	glVertex2d(50, -100);
	glVertex2d(50, -90);
	glVertex2d(60, -100);
	glVertex2d(60, -90);

	glVertex2d(50, -90);
	glVertex2d(50, -80);
	glVertex2d(60, -90);
	glVertex2d(60, -80);

	glVertex2d(60, -90);
	glVertex2d(60, -80);
	glVertex2d(70, -90);
	glVertex2d(70, -80);

	glVertex2d(70, -90);
	glVertex2d(70, -80);
	glVertex2d(80, -90);
	glVertex2d(80, -80);

	glVertex2d(70, -80);
	glVertex2d(70, -70);
	glVertex2d(80, -80);
	glVertex2d(80, -70);

	glEnd();

	glBegin(GL_QUADS); // piece #15
	glColor3d(0.9, 0.4, 0.4);
	glVertex2d(-90, -20);
	glVertex2d(-90, 30);
	glVertex2d(-80, 30);
	glVertex2d(-80, -20);
	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #16
	glColor3d(0.7, 0.7, 0.7);
	glVertex2d(80, -80);
	glVertex2d(80, -70);
	glVertex2d(90, -80);
	glVertex2d(90, -70);

	glVertex2d(80, -100);
	glVertex2d(80, -80);
	glVertex2d(100, -100);
	glVertex2d(100, -80);
	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #17
	glColor3d(0.5, 0.5, 0.7);
	glVertex2d(70, -70);
	glVertex2d(70, -60);
	glVertex2d(80, -70);
	glVertex2d(80, -60);

	glVertex2d(70, -60);
	glVertex2d(70, -50);
	glVertex2d(90, -60);
	glVertex2d(90, -50);

	glVertex2d(80, -50);
	glVertex2d(80, -40);
	glVertex2d(100, -50);
	glVertex2d(100, -40);
	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #18
	glColor3d(0.3, 0.0, 0.0);
	glVertex2d(-80, -90);
	glVertex2d(-80, -80);
	glVertex2d(-70, -90);
	glVertex2d(-70, -80);

	glVertex2d(-80, -80);
	glVertex2d(-80, -70);
	glVertex2d(-50, -80);
	glVertex2d(-50, -70);

	glVertex2d(-60, -90);
	glVertex2d(-60, -80);
	glVertex2d(-50, -90);
	glVertex2d(-50, -80);
	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #19
	glColor3d(0.0, 0.3, 0.0);
	glVertex2d(-40, -90);
	glVertex2d(-40, -80);
	glVertex2d(-30, -90);
	glVertex2d(-30, -80);

	glVertex2d(-40, -80);
	glVertex2d(-40, -70);
	glVertex2d(-30, -80);
	glVertex2d(-30, -70);

	glVertex2d(-50, -80);
	glVertex2d(-50, -70);
	glVertex2d(-40, -80);
	glVertex2d(-40, -70);

	glVertex2d(-40, -70);
	glVertex2d(-40, -60);
	glVertex2d(-20, -70);
	glVertex2d(-20, -60);
	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #20
	glColor3d(0.0, 0.0, 0.3);

	glVertex2d(-60, -60);
	glVertex2d(-60, -50);
	glVertex2d(-50, -60);
	glVertex2d(-50, -50);

	glVertex2d(-50, -50);
	glVertex2d(-50, -40);
	glVertex2d(-40, -50);
	glVertex2d(-40, -40);

	glVertex2d(-50, -60);
	glVertex2d(-50, -50);
	glVertex2d(-40, -60);
	glVertex2d(-40, -50);

	glVertex2d(-40, -60);
	glVertex2d(-40, -50);
	glVertex2d(-30, -60);
	glVertex2d(-30, -50);

	glVertex2d(-50, -70);
	glVertex2d(-50, -60);
	glVertex2d(-40, -70);
	glVertex2d(-40, -60);
	glEnd();

	glBegin(GL_QUAD_STRIP); // piece #21
	glColor3d(0.0, 0.0, 0.5);

	glVertex2d(-90, -70);
	glVertex2d(-90, -60);
	glVertex2d(-80, -70);
	glVertex2d(-80, -60);

	glVertex2d(-80, -70);
	glVertex2d(-80, -50);
	glVertex2d(-70, -70);
	glVertex2d(-70, -50);

	glVertex2d(-70, -70);
	glVertex2d(-70, -60);
	glVertex2d(-50, -70);
	glVertex2d(-50, -60);

	glEnd();*/

	gameBoard.draw();

	// Swap buffers
	glFlush();         // flush the pipeline (usually not necessary)
	glutSwapBuffers(); // we were drawing to the back buffer, now bring it to the front
}

