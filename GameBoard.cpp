#include "GameBoard.h"
#include <vector>
#include <time.h>

GameBoard::GameBoard()
{
	srand(time(NULL));
	gameover = false;
	cout << "Press 'a' or 'd' to rotate pieces, 's' or 'w' to flip through pieces, 'q' to quit" << endl;
	scoreStr = new char[100];
	winnerStr = new char[100];
	activePlayer = 0;
	consecutiveSkips = 0;
	for (int i = 1; i <= 2; i++){ // need to change for loop once you can select how many players
		players.push_back(new Player(i));
	}
	
    // Initialize entire board to -1s
	for (int i = -num_of_row/2; i < num_of_row/2; i++)
	{
		vector<Square*> row;
		for (int j = -num_of_col/2; j < num_of_col / 2; j++)
		{
			row.push_back(new Square(i * 10, j * 10, flag));
		}
		board.push_back(row);
	}
    
    // Place 0s in the main portion of the board
	for (int i = 1; i < 15; i++)
	{
		vector<Square*> nextRow = board[i];
		for (int j = 1; j < 15; j++)
		{
			nextRow[j]->sqNum = 0;
		}
	}
}


GameBoard::~GameBoard()
{
}


void GameBoard::selectPlayer(){
	activePlayer++;
	if (activePlayer > 2)
		activePlayer = 1;
	cout << "Player " << activePlayer << " turn" << endl;
}

bool GameBoard::gameIsNotOver(){
	return !gameover;
}

bool GameBoard::TryPlacePiece(GamePiece* piece)
{
    Vec boardIndex = Vec();
    
    // Get screen cordinates of the piece we are placing
    Vec pieceCoords = piece->getPosition();
    
    // Translate the raw position to the indexes
    boardIndex.x = (pieceCoords.x + 5) / 10 + 7;
    boardIndex.y = num_of_col - (-(pieceCoords.y - 5) / 10 + 7) - 1;
	cout << "BOARDINDEX X " << boardIndex.x << endl;
	cout << "BOARDINDEX Y " << boardIndex.y << endl;
    // STEP 1: Bounds checking. We need to figure out how much space the shape is using in the x and y directions
    for (int i = -2; i <= 2; i++)
    {
        for (int j = -2; j <= 2; j++)
        {
            
            // Look for squares in the piece that are not empty
            if (piece->getData(i+2,j+2) != 0)
            {
                // Check if these non-empty squares are within bounds
                if (boardIndex.x + i < 1)
                    return false;
                if (boardIndex.x + i > 14)
                    return false;
                
                if (boardIndex.y + j < 1)
                    return false;
                if (boardIndex.y + j > 14)
                    return false;
                
            }
        }
    }
    
    // STEP 2: Make sure the piece is not overlapping with any other pieces
    for (int i = -2; i <= 2; i++)
    {
        for (int j = -2; j <= 2; j++)
        {
            // Ignore empty squares for this piece
            if (piece->getData(i+2,j+2) == 0) //fixing so that it accounts for rotation of piece
                continue;
            
            vector<Square*> boardRow = board[boardIndex.x + i];
            
            // Return if there is a piece underneath the piece we are trying to place
            if (boardRow[boardIndex.y + j]->sqNum != 0)
                return false;
        }
    }
    
    // STEP 3: TODO: Make sure the piece follows the game rules
	static int firsttime = players.size();
	bool isCornerPlacement = false;
	bool isValidPlacement = true;
	for (int i = -2; i <= 2; i++)
	{
		for (int j = -2; j <= 2; j++)
		{
			try {
				if (piece->getData(i + 2, j + 2) != GamePiece::EMPTY) {
					if (getSquare(boardIndex.x + i, boardIndex.y + j)->validCornerMask & getActivePlayer()->getPlayerMask()) {
						isCornerPlacement = true;
					}
					if (getSquare(boardIndex.x + i, boardIndex.y + j)->invalidPlacementMask & getActivePlayer()->getPlayerMask()) {
						isValidPlacement = false;
					}
				}
			}
			catch (out_of_range e) {
				// Ignore this one
			}
		}
	}
	if (!isCornerPlacement){
		cout << "NOT CORNER" << endl;
	}
	if (!isValidPlacement){
		cout << "INVALID PLACEMENT" << endl;
	}
	// Not valid corner placement
	if ((!isCornerPlacement || !isValidPlacement) && !firsttime) {
		return false;
	}
	if (firsttime > 0) 
		firsttime -= 1;

    // STEP 4: Place the piece on the board
    for (int i = -2; i <= 2; i++)
    {
        for (int j = -2; j <= 2; j++)
        {
            // Ignore empty squares for this piece
            if (piece->getData(i+2,j+2) == 0)
                continue;
            
            // Update values at this spot
			try{
				getSquare(boardIndex.x + i, boardIndex.y + j)->sqNum = activePlayer;
				getActivePlayer()->score++;
				getSquare(boardIndex.x + i, boardIndex.y + j)->validCornerMask &= 0;
				getSquare(boardIndex.x + i, boardIndex.y + j)->invalidPlacementMask |= 0xFFFFFFFF;
			}
			catch (out_of_range e) {
				// Ignore
			}
		}
    }

	// Set Adjacent as Invalid
	for (int i = -2; i <= 2; i++)
	{
		for (int j = -2; j <= 2; j++)
		{
			if (piece->getData(i + 2, j + 2) & GamePiece::OCCUPIED){
				// Set corners as valid locations
				int dx = -1;
				int dy = 0;
				try {
					getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->validCornerMask &= ~getActivePlayer()->getPlayerMask();
					getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->invalidPlacementMask |= getActivePlayer()->getPlayerMask();
				}
				catch (out_of_range e) {
					// Ignore
				}
				dx = 1;
				dy = 0;
				try {
					getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->validCornerMask &= ~getActivePlayer()->getPlayerMask();
					getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->invalidPlacementMask |= getActivePlayer()->getPlayerMask();
				}
				catch (out_of_range e) {
					// Ignore
				}
				dx = 0;
				dy = 1;
				try {
					getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->validCornerMask &= ~getActivePlayer()->getPlayerMask();
					getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->invalidPlacementMask |= getActivePlayer()->getPlayerMask();
				}
				catch (out_of_range e) {
					// Ignore
				}
				dx = 0;
				dy = -1;
				try {
					getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->validCornerMask &= ~getActivePlayer()->getPlayerMask();
					getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->invalidPlacementMask |= getActivePlayer()->getPlayerMask();
				}
				catch (out_of_range e) {
					// Ignore
				}
			}
		}
	}

	// Set Corners as Valid
	for (int i = -2; i <= 2; i++)
	{
		for (int j = -2; j <= 2; j++)
		{
			if (piece->getData(i + 2, j + 2) & GamePiece::CORNER){
				// Set corners as valid locations
				int dx = -1;
				int dy = -1;
				try {
					if (getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->sqNum == 0 &&
						(getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->invalidPlacementMask & getActivePlayer()->getPlayerMask()) == 0)
					{
						getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->validCornerMask |= getActivePlayer()->getPlayerMask();
					}
				}
				catch (out_of_range e) {
					// Ignore
				}
				dx = 1;
				dy = -1;
				try {
					if (getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->sqNum == 0 &&
						(getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->invalidPlacementMask & getActivePlayer()->getPlayerMask()) == 0)
					{
						getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->validCornerMask |= getActivePlayer()->getPlayerMask();
					}
				}
				catch (out_of_range e) {
					// Ignore
				}
				dx = 1;
				dy = 1;
				try {
					if (getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->sqNum == 0 &&
						(getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->invalidPlacementMask & getActivePlayer()->getPlayerMask()) == 0)
					{
						getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->validCornerMask |= getActivePlayer()->getPlayerMask();
					}
				}
				catch (out_of_range e) {
					// Ignore
				}
				dx = -1;
				dy = 1;
				try {
					if (getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->sqNum == 0 &&
						(getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->invalidPlacementMask & getActivePlayer()->getPlayerMask()) == 0)
					{
						getSquare(boardIndex.x + i + dx, boardIndex.y + j + dy)->validCornerMask |= getActivePlayer()->getPlayerMask();
					}
				}
				catch (out_of_range e) {
					// Ignore
				}
			}
		}
	}
    
    return true;
}

void GameBoard::draw() {
	if (!gameIsNotOver()) {
		drawGameOver();
	}
	//glBegin(GL_QUADS);
	//players.at(activePlayer - 1)->getActivePiece()->draw(); //shows image of piece on mouse before being placed
	//for (int i = 0; i < pieces.size(); i++){
		//pieces.at(i)->draw(); // draws pieces onto the board upon being placed
	//}
	//glEnd();

	//use test for the game pieces being placed on the board
	/*for (int i = 0; i < 16; i++){
		for (int j = 0; j < 16; j++){
		cout << "Board number at coord (" << i << ", " << j << "): " << board.at(i).at(j) << endl;
		}
		}*/
	//glLineWidth(1);
	glColor3d(0.0, 0.0, 1.0);
	for (int i = 0; i < players.size(); i++){
		int j = 0;
		Player * p = players.at(i);
		sprintf(scoreStr, "Player %d Score: %d", p->playerID, p->score);
		glRasterPos2d(-50+50*i, 75);
		while(scoreStr[j] != 0){
			glutBitmapCharacter(GLUT_BITMAP_9_BY_15, scoreStr[j]);
			j++;
		}
	}

	glBegin(GL_LINES);
	//every square is 10 pixels. game board is 100x100 regardless of size.
	for (int i = 0; i < board.size(); i++)
	{
		for (int j = 0; j < board[i].size(); j++)
		{
			if (board[i][j]->validCornerMask & getActivePlayer()->getPlayerMask()) {
				switch (activePlayer)
				{
				case 1:
					glColor3f(0.5, 0.0, 1.0);
					break;
				case 2:
					glColor3f(1.0, 0.8, 0.0);
					break;
				default:
					glColor3f(1.0, 1.0, 1.0);
				}
				glVertex2d(board[i][j]->x+2, board[i][j]->y + 2);
				glVertex2d(board[i][j]->x + 8, board[i][j]->y + 2);
				glVertex2d(board[i][j]->x + 8, board[i][j]->y + 2);
				glVertex2d(board[i][j]->x + 8, board[i][j]->y + 8);
				glVertex2d(board[i][j]->x + 8, board[i][j]->y + 8);
				glVertex2d(board[i][j]->x + 2, board[i][j]->y + 8);
				glVertex2d(board[i][j]->x + 2, board[i][j]->y + 8);
				glVertex2d(board[i][j]->x + 2, board[i][j]->y + 2);
			}
		}
	}
	glEnd();
	//glLineWidth(2);
	glBegin(GL_LINES);
	glColor3f(0.0, 0.0, 0.0); // black dividing lines
	for (int i = -7; i <= 7; i++){
		glVertex2d(i * 10, -70);
		glVertex2d(i * 10, 70);
		glVertex2d(-70, i * 10);
		glVertex2d(70, i * 10);
	}
	glEnd();
    glBegin(GL_QUADS);
	if (gameIsNotOver())
		getActivePlayer()->getActivePiece()->draw();
	for (int i = 0; i < board.size(); i++)
	{
		for (int j = 0; j < board[i].size(); j++)
		{
			switch (board[i][j]->sqNum)
			{
			case -1:
				glColor3f(0.75, 0.75, 0.75);
				break;
			case 1:
				glColor3f(0.5, 0.0, 1.0);
				break;
			case 2:
				glColor3f(1.0, 0.8, 0.0);
				break;
			default:
				glColor3f(1.0, 1.0, 1.0); // white ound
			}
			glVertex2d(board[i][j]->x, board[i][j]->y);
			glVertex2d(board[i][j]->x + 10, board[i][j]->y);
			glVertex2d(board[i][j]->x + 10, board[i][j]->y + 10);
			glVertex2d(board[i][j]->x, board[i][j]->y + 10);
		}
	}
	glEnd();

}
// test for branch
void GameBoard::update(const GlutWindow::Event& e) {
	if (gameIsNotOver()) {
		if (e.type == GlutWindow::MouseDown)
		{
			// Get the piece that the player is trying to place
			GamePiece* nextPiece = players.at(activePlayer - 1)->getActivePiece();

			// Try to copy the piece to the board
			if (TryPlacePiece(nextPiece) == true)
			{
				// Remove the piece from the player's hand
				players.at(activePlayer - 1)->playPiece();

				// End the player's turn
				selectPlayer();

				consecutiveSkips = 0;
			}
		}
		else if (e.type == GlutWindow::Keyboard){
			if (e.key == 'a') {
				players.at(activePlayer - 1)->getActivePiece()->rotate(-90);
				consecutiveSkips = 0;
			}
			else if (e.key == 'd') {
				players.at(activePlayer - 1)->getActivePiece()->rotate(90);
				consecutiveSkips = 0;
			}
			else if (e.key == 's'){
				players.at(activePlayer - 1)->selectPieceDown();
				consecutiveSkips = 0;
			}
			else if (e.key == 'w'){
				players.at(activePlayer - 1)->selectPieceUp();
				consecutiveSkips = 0;
			}
			else if (e.key == 'q'){ //skip player's turn
				selectPlayer();
				consecutiveSkips++;
			}

		}
		for (int i = 0; i < pieces.size(); i++){
			pieces.at(i)->update(e);
		}
		if (e.mx >= 0 && e.my >= 0) // first quadrant
			players.at(activePlayer - 1)->getActivePiece()->setPosition(Vec(e.mx - e.mx % 10 + 5, e.my - e.my % 10 + 5));
		else if (e.mx < 0 && e.my < 0) // third quadrant
			players.at(activePlayer - 1)->getActivePiece()->setPosition(Vec(e.mx - e.mx % 10 - 5, e.my - e.my % 10 - 5));
		else if (e.mx >= 0 && e.my < 0) // 4th quadrant
			players.at(activePlayer - 1)->getActivePiece()->setPosition(Vec(e.mx - e.mx % 10 + 5, e.my - e.my % 10 - 5));
		else //2nd quadrant
			players.at(activePlayer - 1)->getActivePiece()->setPosition(Vec(e.mx - e.mx % 10 - 5, e.my - e.my % 10 + 5));
		if (consecutiveSkips >= players.size()) {
			gameover = true;
			cout << "Game Over!" << endl;
			printScores();
			cout << "SEIZURE WARNING!!!" << endl;
		}
	}
	 else {
		 // Cannot do anything
	 }
}

Player* GameBoard::getActivePlayer() {
	return players.at(activePlayer - 1);
}

Square* GameBoard::getSquare(int x, int y) {
	if (x < 0 || y < 0 || x >= 16 || y >= 16) {
		throw out_of_range("Oops");
	} else {
		return board[x][y];
	}
}

void GameBoard::printScores() {

}

void GameBoard::drawGameOver() {
	
	glColor3d(0.0, 0.0, 1.0);
	Player * p1 = players.at(0);
	Player * p2 = players.at(1);
	if (p1->score > p2->score)
		sprintf(winnerStr, "Player %d Wins!!!", p1->playerID);
	else if (p1->score == p2->score)
		sprintf(winnerStr, "TIE");
	else 
		sprintf(winnerStr, "Player %d Wins!!!", p2->playerID);
	int j = 0;
	glRasterPos2d(-10, 0);
	while (winnerStr[j] != 0){
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, winnerStr[j]);
		j++;
	}

	glBegin(GL_QUADS);
	glColor3f((float)(rand() % 256) / 256, (float)(rand() % 256) / 256, (float)(rand() % 256) / 256);
	glVertex2d(-80, -80);
	glVertex2d(80, -80);
	glVertex2d(80, 80);
	glVertex2d(-80, 80);
	glEnd();
}