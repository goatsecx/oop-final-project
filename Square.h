#pragma once
#include <vector>
using namespace std;

class Square{
public: 
	int x, y;
	int validCornerMask;
	int invalidPlacementMask;
	int sqNum;
	Square(int, int); //initialize squares 
	Square(int, int, int);
	~Square();


};